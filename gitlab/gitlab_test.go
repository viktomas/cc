package gitlab

import (
	"bytes"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/viktomas/cc/git"
)

func TestReturnsValidationErrors(t *testing.T) {

	t.Run("doesn't print any output when commits are valid", func(t *testing.T) {
		r, cleanup := git.TestCreateRepo(t)
		defer cleanup()
		to := git.TestCreateCommit(t, r, "feat: new feature")
		git.TestCreateCommit(t, r, "fix(scope): fixing a bug")
		from := git.TestCreateCommit(t, r, "chore(docs): not this again")

		w := &bytes.Buffer{}

		os.Setenv("CI_MERGE_REQUEST_ID", "1")
		os.Setenv("CI_MERGE_REQUEST_SOURCE_BRANCH_SHA", from)
		os.Setenv("CI_MERGE_REQUEST_DIFF_BASE_SHA", to)

		err := ValidateGitLabMR(r, w)

		require.NoError(t, err)
		require.Len(t, w.String(), 0)
	})

	t.Run("it fails when the commit messages are not valid", func(t *testing.T) {
		r, cleanup := git.TestCreateRepo(t)
		defer cleanup()
		to := git.TestCreateCommit(t, r, "hello new feature")
		git.TestCreateCommit(t, r, "fx(scope): fixing a bug")
		from := git.TestCreateCommit(t, r, "core(docs): not this again")

		w := &bytes.Buffer{}

		os.Setenv("CI_MERGE_REQUEST_ID", "1")
		os.Setenv("CI_MERGE_REQUEST_SOURCE_BRANCH_SHA", from)
		os.Setenv("CI_MERGE_REQUEST_DIFF_BASE_SHA", to)

		err := ValidateGitLabMR(r, w)

		require.Error(t, err)
		require.Regexp(t, `2 validation errors`, err.Error())

		require.Regexp(t, `commit type 'core' is not valid`, w.String())
		require.Regexp(t, `commit type 'fx' is not valid`, w.String())
	})
}
