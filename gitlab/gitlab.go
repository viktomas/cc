package gitlab

import (
	"errors"
	"fmt"
	"io"
	"os"

	"gitlab.com/viktomas/cc/git"
	"gitlab.com/viktomas/cc/validate"
)

func ValidateGitLabMR(path string, w io.Writer) error {

	if os.Getenv("CI_MERGE_REQUEST_ID") == "" {
		return errors.New("the CI_MERGE_REQUEST_ID variable is missing. This command works only inside GitLab CI.")
	}

	sourceBranchSha := os.Getenv("CI_MERGE_REQUEST_SOURCE_BRANCH_SHA")

	var fromCommit string
	if sourceBranchSha != "" {
		fromCommit = sourceBranchSha
	} else {
		headCommit := os.Getenv("CI_COMMIT_SHA")
		if headCommit == "" {
			return errors.New("CI_COMMIT_SHA variable is missing. This shouldn't happen in GitLab CI")
		}
		fromCommit = headCommit
	}

	diffBaseSha := os.Getenv("CI_MERGE_REQUEST_DIFF_BASE_SHA")

	if diffBaseSha == "" {
		return errors.New(
			"CI_MERGE_REQUEST_DIFF_BASE_SHA environment variable is missing." +
				" Is this command running in GitLab CI Merge request pipeline?",
		)
	}

	log, err := git.GetLog(path, fromCommit, diffBaseSha)

	if err != nil {
		return fmt.Errorf("failed to obtain git log from %v, to %v: %w", fromCommit, diffBaseSha, err)
	}

	var errCount int
	for _, ham := range log {
		errors := validate.ParseAndValidate(ham.Message)
		errCount += len(errors)
		if len(errors) > 0 {
			fmt.Fprintf(w, "\n---\n\n🔴 '%s' is not valid\n  commit: %s\n\n", ham.Message, ham.Hash.String())
			for _, e := range errors {
				fmt.Fprintf(w, "  - %s\n", e)
			}
		}
	}
	if errCount > 0 {
		return fmt.Errorf("GitLab MR commit lint failed with %d validation errors", errCount)
	}
	return nil
}
