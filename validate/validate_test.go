package validate

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestParseCommit(t *testing.T) {
	testCases := []struct {
		desc    string
		message string
		exKind  string
		exScope string
		exTitle string
		exBody  string
		exError error
	}{
		{
			desc:    "Can parse full message",
			message: "feat(scope): title of the commit",
			exKind:  "feat",
			exScope: "scope",
			exTitle: "title of the commit",
		},
		{
			desc:    "Can parse message without scope",
			message: "feat: title of the commit",
			exKind:  "feat",
			exTitle: "title of the commit",
		},
		{
			desc:    "Returns error for incorrect message",
			message: "feat:title of the commit",
			exError: errors.New("commit message 'feat:title of the commit' is not valid."),
		},
		{
			desc:    "Can parse multi-paragraph body",
			message: "feat: title\n\nbody1\n\nbody2",
			exKind:  "feat",
			exTitle: "title",
			exBody:  "body1\n\nbody2",
		},
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			result, err := ParseCommit(tC.message)
			require.Equal(t, tC.exError, err)

			require.Equal(t, tC.exKind, result.Kind)
			require.Equal(t, tC.exScope, result.Scope)
			require.Equal(t, tC.exTitle, result.Title)
			require.Equal(t, tC.exBody, result.Body)
		})
	}
}

func TestValidateCommit(t *testing.T) {
	testCases := []struct {
		desc     string
		c        Commit
		exErrors []string
	}{
		{
			desc: "returns no errors for a valid commit",
			c:    Commit{Kind: "feat", Title: "new feature"},
		},
		{
			desc:     "fails validation for unknown kind",
			c:        Commit{Kind: "foo", Title: "new feature"},
			exErrors: []string{"commit type 'foo' is not valid, use one of 'build, ci, docs, feat, fix, perf, refactor, style, test, chore'"},
		},
		{
			desc:     "fails when the title message is too long",
			c:        Commit{Kind: "feat", Title: "this is a very long commit message that will fail the length validation rule"},
			exErrors: []string{"commit title is too long, the maximum allowed length is 72, the actual length is 76"},
		},
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			result := ValidateCommit(tC.c)

			require.Equal(t, len(tC.exErrors), len(result))
			for i := range tC.exErrors {
				require.Equal(t, tC.exErrors[i], result[i].Error())
			}
		})
	}
}
