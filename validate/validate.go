package validate

import (
	"fmt"
	"regexp"
	"strings"
)

type Commit struct {
	Kind  string
	Scope string
	Title string
	Body  string
}

var messageRegexp = regexp.MustCompile(`^(\w+)(?:\(([^)]+)\))?: (.+)(?:\n\n(?s:(.+?)))?(?:\n((?:\n\S+: .+)+))?$`)

func ParseCommit(message string) (Commit, error) {
	results := messageRegexp.FindStringSubmatch(message)
	if results == nil {
		return Commit{}, fmt.Errorf("commit message '%s' is not valid.", message)
	}
	return Commit{
		Kind:  results[1],
		Scope: results[2],
		Title: results[3],
		Body:  results[4],
	}, nil
}

type Rule interface {
	Validate(c Commit) []error
}

func ParseAndValidate(message string) []error {
	trimmedMessage := strings.TrimRight(message, "\n\r")

	c, err := ParseCommit(trimmedMessage)
	if err != nil {
		return []error{err}
	}

	return ValidateCommit(c)
}

func ValidateCommit(c Commit) []error {
	kind := kindInEnumRule{allowedKinds: angularConventionKinds}
	maxLength := tilteMaxLenghtRule{maxLength: 72}
	rules := []Rule{kind, maxLength}
	result := []error{}
	for _, r := range rules {
		errors := r.Validate(c)
		result = append(result, errors...)
	}
	return result
}

type allowedCommitKind struct {
	kind        string
	description string
}

// Taken from
// https://github.com/angular/angular/blob/22b96b9/CONTRIBUTING.md#type
var angularConventionKinds = []string{
	"build",
	"ci",
	"docs",
	"feat",
	"fix",
	"perf",
	"refactor",
	"style",
	"test",
	"chore",
}

type kindInEnumRule struct {
	allowedKinds []string
}

func (r kindInEnumRule) Validate(c Commit) []error {
	for _, ak := range r.allowedKinds {
		if ak == c.Kind {
			return nil
		}
	}

	allowedKinds := []string{}
	for _, ak := range r.allowedKinds {
		allowedKinds = append(allowedKinds, ak)
	}
	return []error{
		fmt.Errorf("commit type '%s' is not valid, use one of '%v'", c.Kind, strings.Join(allowedKinds, ", ")),
	}

}

type tilteMaxLenghtRule struct {
	maxLength int
}

func (r tilteMaxLenghtRule) Validate(c Commit) []error {
	if len(c.Title) > r.maxLength {
		return []error{
			fmt.Errorf("commit title is too long, the maximum allowed length is %d, the actual length is %d", r.maxLength, len(c.Title)),
		}
	}
	return nil

}
