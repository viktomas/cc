APP=cc

.PHONY: build
build:
	go build ./...
	go build -o ${APP} main.go

.PHONY: test
test:
	go test ./...

.PHONY: clean
clean:
	go clean
