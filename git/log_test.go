package git

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestGetLog(t *testing.T) {
	t.Run("works for single commit", func(t *testing.T) {
		r, cleanup := TestCreateRepo(t)
		defer cleanup()
		commitMessage := "Initial Commit\n"
		TestCreateCommit(t, r, commitMessage)

		commitMessages, err := GetLog(r, "", "")

		require.NoError(t, err)
		require.Len(t, commitMessages, 1)
		// removed trailing new line
		require.Equal(t, "Initial Commit", commitMessages[0].Message)
	})
	t.Run("works for multiple commits", func(t *testing.T) {
		r, cleanup := TestCreateRepo(t)
		defer cleanup()
		to := TestCreateCommit(t, r, "1")
		TestCreateCommit(t, r, "2")
		from := TestCreateCommit(t, r, "3")

		commitMessages, err := GetLog(r, from, to)

		require.NoError(t, err)
		require.Len(t, commitMessages, 2)
		require.Equal(t, "3", commitMessages[0].Message)
		require.Equal(t, "2", commitMessages[1].Message)
	})
	/*
		```mermaid
		gitGraph
			commit id: "a"
			commit id: "b"
			branch develop
			checkout develop
			commit id: "c"
			commit id: "d"
			checkout  main
			commit id: "e"
			checkout develop
			branch feature
			checkout feature
			commit id: "f"
			checkout main
			merge develop
			commit id: "g"
		```
	*/
	/*
		```sh
			git commit --allow-empty -m "a"
			git commit --allow-empty -m "b"
			git checkout -b develop
			git commit --allow-empty -m "c"
			git commit --allow-empty -m "d"
			git checkout main
			git commit --allow-empty -m "e"
			git checkout develop
			git checkout -b feature
			git commit --allow-empty -m "f"
			git checkout main
			git merge develop
			git commit --allow-empty -m "g"
		```
	*/
	t.Run("works for multiple branches", func(t *testing.T) {
		r, cleanup := TestCreateRepo(t)
		defer cleanup()
		TestCreateBranch(t, r, "main")
		TestCreateCommit(t, r, "a")
		TestCreateCommit(t, r, "b")
		TestCreateBranch(t, r, "develop")
		c := TestCreateCommit(t, r, "c")
		TestCreateCommit(t, r, "d")
		TestCheckoutBranch(t, r, "main")
		TestCreateCommit(t, r, "e")
		TestCheckoutBranch(t, r, "develop")
		TestCreateBranch(t, r, "feature")
		TestCreateCommit(t, r, "f")
		TestCheckoutBranch(t, r, "main")
		TestMerge(t, r, "develop")
		g := TestCreateCommit(t, r, "g")

		commitMessages, err := GetLog(r, g, c)

		require.NoError(t, err)
		require.Len(t, commitMessages, 4)
		require.Equal(t, "g", commitMessages[0].Message)
		require.Equal(t, "Merge branch 'develop'", commitMessages[1].Message)
		require.Equal(t, "e", commitMessages[2].Message)
		require.Equal(t, "d", commitMessages[3].Message)
	})

	t.Run("works on shallow clone where the to commit is within the depth", func(t *testing.T) {
		origin, originCleanup := TestCreateRepo(t)
		defer originCleanup()
		TestCreateCommit(t, origin, "1")
		to := TestCreateCommit(t, origin, "2")
		TestCreateCommit(t, origin, "3")
		from := TestCreateCommit(t, origin, "4")

		clone, cloneCleanup := TestShallowCloneRepo(t, origin, 3)
		defer cloneCleanup()

		commitMessages, err := GetLog(clone, from, to)

		require.NoError(t, err)
		require.Len(t, commitMessages, 2)
		require.Equal(t, "4", commitMessages[0].Message)
		require.Equal(t, "3", commitMessages[1].Message)
	})

	t.Run("fails when the shallow clone is too shallow to contain the whole diff", func(t *testing.T) {
		origin, originCleanup := TestCreateRepo(t)
		defer originCleanup()
		TestCreateCommit(t, origin, "1")
		to := TestCreateCommit(t, origin, "2")
		TestCreateCommit(t, origin, "3")
		from := TestCreateCommit(t, origin, "4")

		clone, cloneCleanup := TestShallowCloneRepo(t, origin, 2)
		defer cloneCleanup()

		commitMessages, err := GetLog(clone, from, to)

		require.Error(t, err)
		require.Len(t, commitMessages, 0)
	})
}
