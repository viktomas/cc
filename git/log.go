package git

import (
	"fmt"
	"log"
	"strings"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/object"
)

type HashAndMesasge struct {
	Hash    plumbing.Hash
	Message string
}

func GetLog(path, from, to string) ([]HashAndMesasge, error) {
	r, err := git.PlainOpen(path)
	if err != nil {
		return nil, fmt.Errorf("couldn't initialize plain repo in path '%s': %w", path, err)
	}

	// workaround for bug in short ref resolution
	// https://github.com/go-git/go-git/issues/148#issuecomment-1423698285
	// TODO: understand what's the issue based on the comments and maybe contribute fix to the go-git
	r.ResolveRevision(plumbing.Revision(plumbing.ZeroHash.String()))

	var fromHash plumbing.Hash
	if from == "" {
		ref, err := r.Head()
		if err != nil {
			return nil, fmt.Errorf("failed to get repository head: %w", err)
		}
		fromHash = ref.Hash()
	} else {
		fr, err := r.ResolveRevision(plumbing.Revision(from))
		if err != nil {
			return nil, fmt.Errorf("trouble resolving from ref '%s': %w", from, err)
		}

		fromHash = *fr
	}

	var toHash plumbing.Hash
	if to == "" {
		toHash = plumbing.ZeroHash
	} else {

		tr, err := r.ResolveRevision(plumbing.Revision(to))
		if err != nil {
			return nil, fmt.Errorf("trouble resolving to ref '%s': %w", from, err)
		}

		toHash = *tr
	}

	commits, err := partialDiffLog(fromHash, toHash, r)

	if err != nil {
		return nil, fmt.Errorf("failed to get repository log: %w", err)
	}

	messages := []HashAndMesasge{}
	for _, c := range commits {
		trimmedMessage := strings.TrimRight(c.Message, "\n\r")
		messages = append(messages, HashAndMesasge{Hash: c.Hash, Message: trimmedMessage})
	}
	return messages, err

}

func resilientLog(r *git.Repository, sha plumbing.Hash) ([]*object.Commit, error) {
	visited := make(map[plumbing.Hash]struct{})
	var result []*object.Commit
	heads := []plumbing.Hash{sha}
	for len(heads) > 0 {
		head := heads[0]
		heads = heads[1:] // pop
		// if there is a commit that is parent to two different commits
		// (it was branched from), then we want to process it only once
		if _, ok := visited[head]; ok {
			continue
		}
		c, err := r.CommitObject(head)
		if err != nil {
			if err.Error() == "object not found" {
				log.Printf("commit '%v', can't be found, the repository might be shallow clone", head)
				// TODO: return specific errObjectNotFound error
				continue
			}
			return nil, fmt.Errorf("couldn't get commit object for sha %v: %w", sha, err)
		}
		visited[head] = struct{}{}
		result = append(result, c)
		heads = append(heads, c.ParentHashes...)
	}
	return result, nil
}

func partialDiffLog(from, to plumbing.Hash, r *git.Repository) ([]*object.Commit, error) {
	toHashes := make(map[plumbing.Hash]struct{})
	// unless we set the `to`, we use standard r.Log
	if !to.IsZero() {
		toCommits, err := resilientLog(r, to)
		if err != nil {
			return nil, fmt.Errorf("resilient log for 'to' commit failed: %w", err)
		}
		for _, c := range toCommits {
			toHashes[c.Hash] = struct{}{}
		}
	}
	result := []*object.Commit{}
	allCommits, err := resilientLog(r, from)
	if err != nil {
		return nil, fmt.Errorf("calling resilient log failed: %w", err)
	}
	if err != nil {
		return nil, err
	}
	for _, c := range allCommits {
		if _, ok := toHashes[c.Hash]; !ok {
			result = append(result, c)
		}
	}
	return result, nil
}
