package git

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"testing"
)

func TestCreateCommit(t testing.TB, folder, message string) string {
	t.Helper()
	out, err := runCommand(folder, "git", "commit", "--allow-empty", "-m", message)
	if err != nil {
		t.Fatalf("Failed to run git commit: %v", err)
	}
	format := regexp.MustCompile(`^\[.+ (\w+)\].*`)
	if !format.MatchString(out) {
		t.Fatalf("Can't extract commit from the git commit output '%s', expected '[branch sha] message' format", out)
	}
	match := format.FindStringSubmatch(out)
	return match[1]
}

func TestCreateBranch(t testing.TB, folder, name string) {
	t.Helper()
	_, err := runCommand(folder, "git", "checkout", "-b", name)
	if err != nil {
		t.Fatalf("Failed to run git checkout -b: %v", err)
	}
}

func TestMerge(t testing.TB, folder, name string) {
	t.Helper()
	_, err := runCommand(folder, "git", "merge", name)
	if err != nil {
		t.Fatalf("Failed to run git merge: %v", err)
	}
}

func runCommand(dir, cmd string, args ...string) (string, error) {
	prc := exec.Command(cmd, args...)
	stdout := bytes.NewBuffer([]byte{})
	stderr := bytes.NewBuffer([]byte{})
	prc.Stdout = stdout
	prc.Stderr = stderr
	if dir != "" {
		prc.Dir = dir
	}
	err := prc.Run()
	if err != nil {
		return "", err
	}

	if !prc.ProcessState.Success() {
		return "", fmt.Errorf("process didn't return success: %s", stderr.String())
	}
	return stdout.String(), nil
}

// returns path to a temporary repository and a cleanup function
func TestCreateRepo(t testing.TB) (string, func()) {
	folder, err := os.MkdirTemp("", "test-repository")
	if err != nil {
		t.Fatalf("Failed to create a tmp folder: %v", err)
	}
	cleanup := func() {
		err = os.RemoveAll(folder)
		if err != nil {
			t.Fatalf("failed to clean up a repository directory: %v", err)
		}
	}

	// init git repository
	out, err := runCommand(folder, "git", "init")
	if err != nil {
		t.Errorf("failed initializing repository in '%s': %v, output: %s", folder, err, out)
		cleanup()
		t.FailNow()
	}

	return folder, cleanup
}

// origin is a path to local folder that contains repository for the shallow clone
func TestShallowCloneRepo(t testing.TB, origin string, depth int) (string, func()) {
	folder, err := os.MkdirTemp("", "test-clone-repository")
	if err != nil {
		t.Fatalf("Failed to create a tmp folder: %v", err)
	}
	cleanup := func() {
		err = os.RemoveAll(folder)
		if err != nil {
			t.Fatalf("failed to clean up a repository directory: %v", err)
		}
	}

	// clone
	out, err := runCommand(folder, "git", "clone", "--depth", strconv.Itoa(depth), fmt.Sprintf("file://%s", origin), folder)
	if err != nil {
		t.Errorf("failed initializing repository in '%s': %v, output: %s", folder, err, out)
		cleanup()
		t.FailNow()
	}

	return folder, cleanup
}

func TestCheckoutBranch(t testing.TB, folder, name string) {
	t.Helper()
	_, err := runCommand(folder, "git", "checkout", name)
	if err != nil {
		t.Fatalf("Failed to run git checkout: %v", err)
	}
}
