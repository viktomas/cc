package main

import (
	"os"

	"gitlab.com/viktomas/cc/cmd"
)

func main() {
	if err := cmd.Execute(); err != nil {
		os.Exit(1)
	}
}
