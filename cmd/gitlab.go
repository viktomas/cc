package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/viktomas/cc/gitlab"
)

func init() {
	rootCmd.AddCommand(gitlabValidateCmd)
}

var gitlabValidateCmd = &cobra.Command{
	Use:   "gl-validate",
	Short: "validates GitLab MR commits",
	Long:  `validates that all commits in GitLab MR are according to the conventional commit specification`,
	Args:  cobra.MaximumNArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		repoPath := "."
		if len(args) > 0 {
			repoPath = args[0]
		}
		return gitlab.ValidateGitLabMR(repoPath, cmd.OutOrStderr())
	},
}
