package cmd

import (
	"bytes"
	"regexp"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/viktomas/cc/git"
)

func TestValidateCmd(t *testing.T) {
	t.Run("doesn't print any output when commits are valid", func(t *testing.T) {
		r, cleanup := git.TestCreateRepo(t)
		defer cleanup()
		to := git.TestCreateCommit(t, r, "feat: new feature")
		git.TestCreateCommit(t, r, "fix(scope): fixing a bug")
		from := git.TestCreateCommit(t, r, "chore(docs): not this again")

		w := &bytes.Buffer{}

		err := validateCommits(r, from, to, w)

		require.NoError(t, err)
		require.Len(t, w.String(), 0)
	})

	t.Run("it ignores commits from outside of the from-to range", func(t *testing.T) {
		r, cleanup := git.TestCreateRepo(t)
		defer cleanup()
		git.TestCreateCommit(t, r, "hello new feature")
		to := git.TestCreateCommit(t, r, "fix(scope): fixing a bug")
		from := git.TestCreateCommit(t, r, "chore(docs): not this again")
		git.TestCreateCommit(t, r, "hello new feature2")

		w := &bytes.Buffer{}

		err := validateCommits(r, from, to, w)

		require.NoError(t, err)
		require.Len(t, w.String(), 0)
	})

	t.Run("it fails when the commit messages are not valid", func(t *testing.T) {
		r, cleanup := git.TestCreateRepo(t)
		defer cleanup()
		to := git.TestCreateCommit(t, r, "hello new feature")
		git.TestCreateCommit(t, r, "fx(scope): fixing a bug")
		from := git.TestCreateCommit(t, r, "core(docs): not this again")
		w := &bytes.Buffer{}

		err := validateCommits(r, from, to, w)
		require.Error(t, err)
		require.Regexp(t, `2 validation errors`, err.Error())

		require.Regexp(t, `commit type 'core' is not valid`, w.String())
		require.Regexp(t, `commit type 'fx' is not valid`, w.String())
	})

}
func TestPresenting(t *testing.T) {
	t.Run("presents the validation errors", func(t *testing.T) {
		r, cleanup := git.TestCreateRepo(t)
		defer cleanup()
		git.TestCreateCommit(t, r, "fx(scope): fixing a bug")
		w := &bytes.Buffer{}

		err := validateCommits(r, "", "", w)
		require.Error(t, err)
		sanitizedError := regexp.MustCompile(`\[[\da-f]{7}\]`).ReplaceAllString(w.String(), `[abcdef1]`)
		require.Equal(t, `- 🔴 [abcdef1]: 'fx(scope): fixing a bug' is not valid
  - commit type 'fx' is not valid, use one of 'build, ci, docs, feat, fix, perf, refactor, style, test, chore'
`, sanitizedError)
	})

	t.Run("shows only the first line of the commit message", func(t *testing.T) {
		r, cleanup := git.TestCreateRepo(t)
		defer cleanup()
		git.TestCreateCommit(t, r, "fx(scope): fixing a bug\n\nand this is second line")
		w := &bytes.Buffer{}

		err := validateCommits(r, "", "", w)
		require.Error(t, err)
		sanitizedError := regexp.MustCompile(`\[[\da-f]{7}\]`).ReplaceAllString(w.String(), `[abcdef1]`)
		require.Equal(t, `- 🔴 [abcdef1]: 'fx(scope): fixing a bug' is not valid
  - commit type 'fx' is not valid, use one of 'build, ci, docs, feat, fix, perf, refactor, style, test, chore'
`, sanitizedError)
	})

	t.Run("pads the next error lines", func(t *testing.T) {
		r, cleanup := git.TestCreateRepo(t)
		defer cleanup()
		git.TestCreateCommit(t, r, "(scope): fixing a bug\n\nand this is second line")
		w := &bytes.Buffer{}

		err := validateCommits(r, "", "", w)
		require.Error(t, err)
		sanitizedError := regexp.MustCompile(`\[[\da-f]{7}\]`).ReplaceAllString(w.String(), `[abcdef1]`)
		require.Equal(t, `- 🔴 [abcdef1]: '(scope): fixing a bug' is not valid
  - commit message '(scope): fixing a bug
    
    and this is second line' is not valid.
`, sanitizedError)
	})
}
