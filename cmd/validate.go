package cmd

import (
	"errors"
	"fmt"
	"io"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/viktomas/cc/git"
	"gitlab.com/viktomas/cc/validate"
)

func init() {
	rootCmd.AddCommand(validateCmd)
	validateCmd.PersistentFlags().String("from", "", "The most recent commit to start from")
	validateCmd.PersistentFlags().String("to", "", "The oldest commit to still validate")
}

var validateCmd = &cobra.Command{
	Use:          "validate",
	Short:        "validates commits",
	Long:         `validates that all commits are according to the conventional commit specification`,
	SilenceUsage: true,
	Args:         cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) != 1 {
			return errors.New("you have to pass in a folder with repository")
		}
		from, _ := cmd.Flags().GetString("from")
		to, _ := cmd.Flags().GetString("to")
		err := validateCommits(args[0], from, to, cmd.OutOrStderr())
		if err != nil {
			return err
		}
		return nil
	},
}

type invalidCommit struct {
	ham    git.HashAndMesasge
	errors []error
}

func padItem(item string) string {
	lines := strings.Split(item, "\n")
	resultLines := []string{fmt.Sprintf("  - %s", lines[0])}
	for _, l := range lines[1:] {
		resultLines = append(resultLines, fmt.Sprintf("    %s", l))
	}
	return strings.Join(resultLines, "\n")
}

func validateCommits(path, from, to string, w io.Writer) error {
	log, err := git.GetLog(path, from, to)
	if err != nil {
		return fmt.Errorf("failed to obtain git log from %v, to %v: %w", from, to, err)
	}
	results := []invalidCommit{}
	for _, ham := range log {
		errors := validate.ParseAndValidate(ham.Message)

		if len(errors) > 0 {
			results = append(results, invalidCommit{ham: ham, errors: errors})
		}
	}

	for _, ic := range results {
		shortHash := ic.ham.Hash.String()[:7]
		commitFirstLine := strings.Split(ic.ham.Message, "\n")[0]
		fmt.Fprintf(w, "- 🔴 [%s]: '%s' is not valid\n", shortHash, commitFirstLine)
		for _, e := range ic.errors {
			fmt.Fprint(w, padItem(e.Error()))
			fmt.Fprint(w, "\n")
		}
	}
	if len(results) > 0 {
		return fmt.Errorf("Commit lint failed with %d validation errors", len(results))
	}
	return nil
}
