package cmd

import (
	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "cc",
	Short: "conventional-commits",
	Long:  `conventional commit tooling`,
}

func Execute() error {
	return rootCmd.Execute()
}
